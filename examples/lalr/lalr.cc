#include <liblalr/parser.h>

#include <libmeta/meta.h>

#include <iostream>
#include <string>
#include <string_view>
#include <variant>
#include <vector>

namespace
{

// Terminals.

    template<char Ch>
    struct Char
    {
        friend std::ostream& operator <<(std::ostream& os, Char)
        {
            return os << "Char<" << Ch <<  '>';
        }
    };

    struct Digit
    {
        friend std::ostream& operator <<(std::ostream& os, Digit const& digit)
        {
            return os << "Digit{" << digit.ch << '}';
        }

        char ch{};
    };

    struct Initial
    {
        friend std::ostream& operator <<(std::ostream& os, Initial const& initial)
        {
            return os << "Initial{" << initial.ch << '}';
        }

        char ch{};
    };

    struct Other
    {
        friend std::ostream& operator <<(std::ostream& os, Other const& other)
        {
            return os << "Other{" << other.ch << '}';
        }

        char ch{};
    };

// Nonterminals.

    struct Ws
    {
        template<char Ch>
        constexpr explicit Ws(Char<Ch>) noexcept {}
    };

    struct WsSeq
    {
        constexpr WsSeq() noexcept = default;
        constexpr WsSeq(WsSeq, Ws) noexcept {}
    };

    struct IdChar
    {
        constexpr explicit IdChar(Digit const& digit) noexcept: ch{digit.ch} {}
        constexpr explicit IdChar(Initial const& initial) noexcept: ch{initial.ch} {}

        char ch{};
    };

    struct IdCharSeq
    {
        IdCharSeq() = default;

        IdCharSeq(IdCharSeq lhs, IdChar const& rhs)
        : id{std::move(lhs.id)}
        {
            id.push_back(rhs.ch);
        }

        std::string id{};
    };

    struct Identifier
    {
        Identifier(Initial const& initial, IdCharSeq const& seq)
        : id(1, initial.ch)
        {
            id.append(seq.id);
        }

        std::string id{};
    };

    struct EscapedChar
    {
        EscapedChar(Digit const& d0, Digit const& d1, Digit const& d2)
        : ch{d0.ch, d1.ch, d2.ch}
        {}

        template<char Ch>
        explicit EscapedChar(Char<Ch>): ch(1, Ch) {}
        explicit EscapedChar(Initial const& initial): ch(1, initial.ch) {}
        explicit EscapedChar(Other const& other): ch(1, other.ch) {}

        std::string ch{};
    };

    struct LiteralChar
    {
        LiteralChar(Char<'\\'>, EscapedChar const& escaped)
        : ch(1, '\\')
        {
            ch.append(escaped.ch);
        }

        template<char Ch>
        LiteralChar(Char<Ch>): ch(1, Ch) {}
        LiteralChar(Digit const& digit): ch(1, digit.ch) {}
        LiteralChar(Initial const& initial): ch(1, initial.ch) {}
        LiteralChar(Other const& other): ch(1, other.ch) {}

        std::string ch{};
    };

    struct Literal
    {
        Literal(Char<'\''>, LiteralChar const& literalChar, Char<'\''>)
        : literal(2, '\'')
        {
            literal.insert(
                1 + literal.begin(),
                literalChar.ch.begin(),
                literalChar.ch.end()
            );
        }

        std::string literal{};
    };

    struct Symbol
    {
        explicit Symbol(Identifier identifier): symbol{std::move(identifier.id)} {}
        explicit Symbol(Literal literal): symbol{std::move(literal.literal)} {}

        std::string symbol{};
    };

    struct RBody
    {
        RBody() = default;
        explicit RBody(Symbol const& symbol): symbols(1, symbol.symbol) {}

        RBody(Symbol const& symbol, Ws, WsSeq, RBody const& rbody)
        : symbols(1, symbol.symbol)
        {
            symbols.insert(symbols.end(), rbody.symbols.begin(), rbody.symbols.end());
        }

        friend std::ostream& operator <<(std::ostream& os, RBody const& rbody)
        {
            std::string_view sep{""};
            for (auto const& symbol : rbody.symbols) {
                os << sep << symbol;
                sep = " ";
            }
            return os;
        }

        std::vector<std::string> symbols{};
    };

    struct RBodySeq
    {
        RBodySeq(RBody const& rbody, Char<';'>, WsSeq): rbodies(1, rbody) {}

        RBodySeq(RBody const& rbody, Char<'|'>, WsSeq, RBodySeq const& rbodyseq)
        : rbodies(1, rbody)
        {
            rbodies.insert(rbodies.end(), rbodyseq.rbodies.begin(), rbodyseq.rbodies.end());
        }

        std::vector<RBody> rbodies{};
    };

    struct Rule
    {
        Rule(Identifier identifier, WsSeq, Char<':'>, WsSeq, RBodySeq rbodyseq)
        : id{std::move(identifier.id)}
        , rbodies{std::move(rbodyseq.rbodies)}
        {}

        friend std::ostream& operator <<(std::ostream& os, Rule const& rule)
        {
            os << rule.id << " : ";
            std::string sep{""};
            for (auto const& rbody : rule.rbodies) {
                os << sep << rbody;
                if (sep.empty()) {
                    sep = '\n' + std::string(rule.id.size(), ' ') + " | ";
                }
            }
            return os << '\n' << std::string(rule.id.size(), ' ') << " ;\n";
        }

        std::string id{};
        std::vector<RBody> rbodies{};
    };

    struct RuleSeq
    {
        explicit RuleSeq(Rule const& rule): rules(1, rule) {}

        RuleSeq(Rule const& rule, RuleSeq const& ruleseq)
        : rules(1, rule)
        {
            rules.insert(rules.end(), ruleseq.rules.begin(), ruleseq.rules.end());
        }

        std::vector<Rule> rules{};
    };

    struct Program
    {
        Program(WsSeq, RuleSeq ruleseq): rules{std::move(ruleseq.rules)} {}

        friend std::ostream& operator <<(std::ostream& os, Program const& program)
        {
            std::copy(
                program.rules.begin(),
                program.rules.end(),
                std::ostream_iterator<Rule>(os,
                "\n")
            );
            return os;
        }

        std::vector<Rule> rules{};
    };

// Grammar.

    using lalr::Production;
    using Productions =
        meta::list<
            Production<Program, WsSeq, RuleSeq>,

            Production<RuleSeq, Rule, RuleSeq>,
            Production<RuleSeq, Rule>,

            Production<Rule, Identifier, WsSeq, Char<':'>, WsSeq, RBodySeq>,

            Production<RBodySeq, RBody, Char<';'>, WsSeq>,
            Production<RBodySeq, RBody, Char<'|'>, WsSeq, RBodySeq>,

            Production<RBody, Symbol, Ws, WsSeq, RBody>,
            Production<RBody, Symbol>,
            Production<RBody>,

            Production<Symbol, Identifier>,
            Production<Symbol, Literal>,

            Production<Literal, Char<'\''>, LiteralChar, Char<'\''>>,

            Production<LiteralChar, Char<' '>>,
            Production<LiteralChar, Char<':'>>,
            Production<LiteralChar, Char<';'>>,
            Production<LiteralChar, Char<'\\'>, EscapedChar>,
            Production<LiteralChar, Char<'\t'>>,
            Production<LiteralChar, Char<'|'>>,
            Production<LiteralChar, Digit>,
            Production<LiteralChar, Initial>,
            Production<LiteralChar, Other>,

            Production<EscapedChar, Digit, Digit, Digit>,
            Production<EscapedChar, Initial>,
            Production<EscapedChar, Other>,
            Production<EscapedChar, Char<'\''>>,
            Production<EscapedChar, Char<'\\'>>,

            Production<Identifier, Initial, IdCharSeq>,

            Production<IdCharSeq, IdCharSeq, IdChar>,
            Production<IdCharSeq>,

            Production<IdChar, Digit>,
            Production<IdChar, Initial>,

            Production<WsSeq, WsSeq, Ws>,
            Production<WsSeq>,

            Production<Ws, Char<' '>>,
            Production<Ws, Char<'\f'>>,
            Production<Ws, Char<'\n'>>,
            Production<Ws, Char<'\t'>>,
            Production<Ws, Char<'\v'>>
        >;

// Tokeniser.

    int Main(std::vector<std::string_view>)
    {
        lalr::Parser<Productions, Program> parser{};

        char ch{};
        while (std::cin.get(ch)) {
            if (std::isdigit(ch)) {
                parser.token(Digit{static_cast<char>(ch - '0')});
            } else if (std::isalpha(ch) || '.' == ch || '_' == ch) {
                parser.token(Initial{ch});
            } else {
                switch (ch) {
                case ' ': parser.token(Char<' '>{}); break;
                case ':': parser.token(Char<':'>{}); break;
                case ';': parser.token(Char<';'>{}); break;
                case '\'': parser.token(Char<'\''>{}); break;
                case '\\': parser.token(Char<'\\'>{}); break;
                case '\f': parser.token(Char<'\f'>{}); break;
                case '\n': parser.token(Char<'\n'>{}); break;
                case '\t': parser.token(Char<'\t'>{}); break;
                case '\v': parser.token(Char<'\v'>{}); break;
                case '|': parser.token(Char<'|'>{}); break;
                default: parser.token(Other{ch}); break;
                }
            }
        }

        if (parser.finish()) {
            std::cout << std::get<Program>(parser.value()) << '\n';
            return 0;
        } else {
            std::cerr << "Parse error.\n";
            return 1;
        }
    }
}

int
main(int
c,char**v){return
Main({v,c+v});}
