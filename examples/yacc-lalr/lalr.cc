#include "dstring.h"
#include "lalr.h"
#include "rule.h"
#include "streamer.h"
#include "subset.h"

#include <algorithm>
#include <cassert>
#include <cwchar>
#include <iostream>
#include <numeric>
#include <set>
#include <string_view>
#include <tuple>
#include <vector>

using streamer::stream;

namespace
{

/**
 * Extract the list of symbols from a rule body.
 *
 * @param rbody The rule body of a production.
 * @return The list of symbols.
 */

    std::vector<std::string_view> symbols(rbody const* rbody)
    {
        std::vector<std::string_view> result{};

        rbody_each(
            rbody,
            &result,
            [](string const* string, void* closure)
            {
                static_cast<decltype(result)*>(closure)->emplace_back(
                    string->begin,
                    string->size
                );
            }
        );

        return result;
    }

/**
 * I'm a parsed grammar.
 */

    struct Grammar
    {

/**
 * Productions are represented by a left- and right-hand-side.
 */

        using Production = std::tuple<std::string_view, std::vector<std::string_view>>;

/**
 * Create a grammar from a list of productions.
 *
 * The first production is assumed to be the starting rule.
 *
 * @param ruleList The list of productions.
 */

        explicit Grammar(rule_list const& ruleList)
        {
            rule_list_each(
                &ruleList,
                this,
                [](string const* const identifier, rbody const* rbody, void* closure)
                {
                    auto& result{*static_cast<Grammar*>(closure)};
                    std::string_view const id{identifier->begin, identifier->size};

// By convention, the first production is the starting rule.

                    if (result.start.empty()) {
                        result.start = "S";
                        result.productions.push_back({"S", {id}});
                    }

                    result.productions.emplace_back(id, symbols(rbody));
                }
            );
            std::sort(productions.begin(), productions.end());
        }

/**
 * @return An iterator to the first production for the given symbol.
 */

        auto firstProduction(std::string_view const& symbol) const
        {
            return std::lower_bound(
                productions.begin(),
                productions.end(),
                Production{symbol, {}}
            );
        }

        std::string_view start{};
        std::vector<Production> productions{};
    };

/**
 * Determine the number of code points in a multi-byte string.
 */

    std::size_t mbslen(std::string_view const& str)
    {
        std::mbstate_t mbstate{};
        std::size_t count{0};
        for (std::size_t n{0}; n != str.size(); ++count) {
            std::size_t const r{std::mbrlen(&str[n], str.size() - n, &mbstate)};
            if (static_cast<std::size_t>(-2) <= r) {
                fprintf(stderr, "stdin: encoding error.\n");
                exit(1);
            }
            n += r;
        }
        return count;
    }

/**
 * A non-deterministic finite automaton representing a grammar.
 */

    class Nfa
    {
    public:
        using State = std::size_t;
        using Label = std::string_view;

        explicit Nfa(Grammar const& grammar)
        : mGrammar{grammar}
        {}

/**
 * @return The production's initial state.
 */

        std::size_t initialState(std::size_t productionIndex) const
        {
            return std::accumulate(
                &mGrammar.productions[0],
                &mGrammar.productions[productionIndex],
                std::size_t{0},
                [](std::size_t const state, Grammar::Production const& prdctn) {
                    return state + 1 + std::get<1>(prdctn).size();
                }
            );
        }

/**
 * @return The states representing the grammar's starting state.
 */

        std::vector<State> start() const
        {
            auto prdctn{mGrammar.firstProduction(mGrammar.start)};
            std::size_t const productionIndex{static_cast<std::size_t>(prdctn - mGrammar.productions.begin())};
            std::size_t state{initialState(productionIndex)};

            std::vector<State> result{};
            while (
                (prdctn != mGrammar.productions.end()) &&
                (mGrammar.start == std::get<0>(*prdctn))
            ) {
                result.push_back(state);
                state += 1 + std::get<1>(*prdctn).size();
                ++prdctn;
            }

            return result;
        }

/**
 * @return The closure of the given states.
 */

        std::set<State> closure(std::vector<State> states) const
        {
            std::set<State> closureStates{states.begin(), states.end()};

            while (!states.empty()) {
                State const state{states.back()};
                states.pop_back();

                auto const productionAndIndex{production(state)};
                Grammar::Production const& prdctn{std::get<0>(productionAndIndex)};

// Inner states of a production can transition to any of the initial states
// of the subsequent nonterminal.

                std::size_t const i{std::get<1>(productionAndIndex)};
                if (i < std::get<1>(prdctn).size()) {

// Add the start state of each subsequent nonterminal.

                    auto const& symbol{std::get<1>(prdctn)[i]};
                    auto it{mGrammar.firstProduction(symbol)};
                    std::size_t newState{initialState(static_cast<std::size_t>(it - mGrammar.productions.begin()))};
                    while (
                        (it != mGrammar.productions.end()) &&
                        (symbol == std::get<0>(*it))
                    ) {
                        if (closureStates.insert(newState).second) {
                            states.push_back(newState);
                        }
                        newState += 1 + std::get<1>(*it).size();
                        ++it;
                    }
                }
            }

            return closureStates;
        }

/**
 * @return The production numbers that this state reduces to.
 * @note A state can reduce to zero or one productions.
 */

        std::vector<std::size_t> reductions(State const& state) const
        {
            auto const productionAndIndex{productionIndex(state)};
            std::size_t const n{std::get<0>(productionAndIndex)};
            assert(n < mGrammar.productions.size());
            Grammar::Production const& prdctn{mGrammar.productions[n]};

            std::size_t const i{std::get<1>(productionAndIndex)};
            assert(i <= std::get<1>(prdctn).size());
            if (i == std::get<1>(prdctn).size()) {
                return {n};
            }

            return {};
        }

/**
 * @return The edges from the given state.
 * @note There can be zero or one edges from a given state.
 */

        std::vector<std::tuple<Label, State>> edges(State const& state) const
        {
            auto const productionAndIndex{production(state)};
            Grammar::Production const& prdctn{std::get<0>(productionAndIndex)};

            std::size_t const i{std::get<1>(productionAndIndex)};
            assert(i <= std::get<1>(prdctn).size());
            if (i < std::get<1>(prdctn).size()) {
                return {{std::get<1>(prdctn)[i], 1 + state}};
            }

            return {};
        }

    private:
        std::tuple<std::size_t, std::size_t> productionIndex(State state) const
        {
            std::size_t index{0};
            while (
                (index != mGrammar.productions.size()) &&
                (std::get<1>(mGrammar.productions[index]).size() < state)
            ) {
                state -= (1 + std::get<1>(mGrammar.productions[index]).size());
                ++index;
            }
            assert(index != mGrammar.productions.size());
            return {index, state};
        }

        std::tuple<Grammar::Production const&, std::size_t> production(State state) const
        {
            auto const productionAndIndex{productionIndex(state)};
            return {
                mGrammar.productions[std::get<0>(productionAndIndex)],
                std::get<1>(productionAndIndex)
            };
        }

    private:
        Grammar const& mGrammar;
    };

} // namespace <anonymous>

void lalr(rule_list const* ruleList)
{
    Grammar const grammar{*ruleList};

// Output the grammar in topological order.

    std::set<std::string_view> needed{grammar.start};
    std::set<std::string_view> done{};
    while (!needed.empty()) {
        std::string_view const& id{*needed.begin()};
        std::string const whitespace(mbslen(id), ' ');

        auto const from{grammar.firstProduction(id)};
        auto it{from};
        while (it != grammar.productions.end() && id == std::get<0>(*it)) {
            if (from == it) {
                std::cout << id << " :";
            } else {
                std::cout << whitespace << " |";
            }
            for (auto const& symbol : std::get<1>(*it)) {
                std::cout << ' ' << symbol;
                if (0 == done.count(symbol)) {
                    needed.insert(symbol);
                }
            }
            std::cout << '\n';
            ++it;
        }
        if (from != it) {
            std::cout << whitespace << " ;\n\n";
        }

        done.insert(id);
        needed.erase(id);
    }

// Build a deterministic automaton that recognises the grammar.

    Nfa nfa{grammar};
    std::cerr << subset::createDfa(nfa);
}
