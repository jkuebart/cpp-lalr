#ifndef STRING_POOL_H
#define STRING_POOL_H

#include <stddef.h>

/**
 * Provide a string pool location with sufficient capacity.
 *
 * @param n Number of required characters.
 * @return A string pool location with at least the requested capacity.
 */
char *string_pool_reserve(size_t n);

/**
 * Add a sequence of characters to the string pool.
 *
 * If the appropriate capacity has been made available using
 * string_pool_reserve(), this function will return the same location and
 * successive calls are guaranteed to produce a contiguous string.
 *
 * @param begin Start of the sequence.
 * @param size Size of the sequence.
 * @return Location of the character sequence in the pool.
 */
char const *string_pool_add(char const *begin, size_t size);

#endif // STRING_POOL_H
