#include "rule.h"

#include "dstring.h"

#include <stdlib.h>
#include <stdio.h>

/**
 * A rule body is represented by a list of strings.
 */
struct rbody
{
    struct rbody const *next;
    struct string string;
};

struct rbody const *
rbody_new(
    struct string const *const string,
    struct rbody const *const tail
)
{
    struct rbody *result = malloc(sizeof(*result));
    result->next = tail;
    result->string = *string;
    return result;
}

void
rbody_each(
    struct rbody const *rbody,
    void *const closure,
    void (*func)(struct string const *string, void *closure)
)
{
    while (rbody) {
        func(&rbody->string, closure);
        rbody = rbody->next;
    }
}

/**
 * A sequence of rule bodies represent alternative productions.
 */
struct rbody_seq
{
    struct rbody_seq const *next;
    struct rbody const *rbody;
};

struct rbody_seq const *
rbody_seq_new(
    struct rbody const *const rbody,
    struct rbody_seq const *const tail
)
{
    struct rbody_seq *result = malloc(sizeof(*result));
    result->next = tail;
    result->rbody = rbody;
    return result;
}

/**
 * A list of productions.
 */
struct rule_list
{
    struct rule_list const *next;
    struct string identifier;
    struct rbody const *rbody;
};

struct rule_list const *
rule_list_new(
    struct string const *const identifier,
    struct rbody const *const rbody,
    struct rule_list const *const tail
)
{
    struct rule_list *result = malloc(sizeof(*result));
    result->next = tail;
    result->identifier = *identifier;
    result->rbody = rbody;
    return result;
}

void
rule_list_each(
    struct rule_list const *rule_list,
    void *const closure,
    void (*func)(struct string const *identifier, struct rbody const *rbody, void *closure)
)
{
    while (rule_list) {
        func(&rule_list->identifier, rule_list->rbody, closure);
        rule_list = rule_list->next;
    }
}

struct rule_list const *
make_rule_list(
    struct string const *const identifier,
    struct rbody_seq const *rbody_seq
)
{
    struct rule_list const *result = NULL;
    while (rbody_seq) {
        result = rule_list_new(identifier, rbody_seq->rbody, result);
        rbody_seq = rbody_seq->next;
    }
    return result;
}

struct rule_list const *
rule_list_concat(struct rule_list const *lhs, struct rule_list const *rhs)
{
    struct rule_list const *result = NULL;
    while (rhs) {
        result = rule_list_new(&rhs->identifier, rhs->rbody, result);
        rhs = rhs->next;
    }
    while (lhs) {
        result = rule_list_new(&lhs->identifier, lhs->rbody, result);
        lhs = lhs->next;
    }
    return result;
}
