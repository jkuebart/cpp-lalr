#include "string_pool.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static size_t const MIN_POOL_SIZE = 1024;

struct string_pool
{
    char *end, *capacity;
    char pool[];
};

static struct string_pool *string_pool;

char *
string_pool_reserve(size_t const n)
{
    if (!string_pool || string_pool->capacity < n + string_pool->end) {
        size_t const new_size = n < MIN_POOL_SIZE ? MIN_POOL_SIZE : n;
        string_pool = malloc(new_size + sizeof(*string_pool));
        if (!string_pool) {
            perror("string_pool");
            exit(1);
        }
        string_pool->end = string_pool->pool;
        string_pool->capacity = new_size + string_pool->pool;
    }
    return string_pool->end;
}

char const *
string_pool_add(char const *const begin, size_t const size)
{
    char *const location = string_pool_reserve(size);
    memmove(location, begin, size);
    string_pool->end += size;
    return location;
}
