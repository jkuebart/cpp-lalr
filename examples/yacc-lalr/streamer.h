#ifndef STREAMER_H
#define STREAMER_H

#include <iterator>
#include <ostream>

namespace streamer
{
    template<typename Iterator, typename Sentinel>
    class Streamer
    {
    public:
        Streamer(Iterator from, Sentinel to)
        : mFrom{std::move(from)}
        , mTo{std::move(to)}
        {}

        friend std::ostream& operator <<(std::ostream& os, Streamer const& streamer)
        {
            char const* sep = "";
            for (auto it = streamer.mFrom; it != streamer.mTo; ++it)
            {
                os << sep << *it;
                sep = " ";
            }
            return os;
        }

    private:
        Iterator mFrom{};
        Sentinel mTo{};
    };

    template<typename Iterator, typename Sentinel>
    auto streamFromTo(Iterator from, Sentinel to)
    {
        return Streamer{std::move(from), std::move(to)};
    }

    template<typename Container>
    auto stream(Container&& cont)
    {
        using std::begin;
        using std::end;
        return streamFromTo(begin(cont), end(cont));
    }
} // namespace streamer

#endif // STREAMER_H
