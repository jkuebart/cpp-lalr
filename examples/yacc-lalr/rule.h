#ifndef RULE_H
#define RULE_H

#ifdef __cplusplus
extern "C" {
#endif

struct rbody;
struct rbody_seq;
struct rule_list;
struct string;

/**
 * Allocate and initialise a new rbody node.
 */
struct rbody const *
rbody_new(struct string const *string, struct rbody const *tail);

/**
 * Call the function for every term.
 */
void
rbody_each(
    struct rbody const *rbody,
    void *closure,
    void (*)(struct string const *string, void *closure)
);

/**
 * Allocate and initialise a new rbody_seq node.
 */
struct rbody_seq const *
rbody_seq_new(struct rbody const *rbody, struct rbody_seq const *tail);

/**
 * Allocate and initialise a new rule_list node.
 */
struct rule_list const *
rule_list_new(
    struct string const *identifier,
    struct rbody const *rbody,
    struct rule_list const *tail
);

/**
 * Call the function for every production.
 */
void
rule_list_each(
    struct rule_list const *rule_list,
    void *closure,
    void (*)(struct string const *identifier, struct rbody const *rbody, void *closure)
);

/**
 * Create a production for each rule body in the sequence.
 */
struct rule_list const *
make_rule_list(struct string const *identifier, struct rbody_seq const *rbody_seq);

struct rule_list const *
rule_list_concat(struct rule_list const *lhs, struct rule_list const *rhs);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // RULE_H
