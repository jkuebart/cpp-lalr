#include "dstring.h"

#include "string_pool.h"

#include <string.h>

struct string
string_char(char const ch)
{
    return (struct string){string_pool_add(&ch, 1), 1};
}

struct string
string_str(char const *const str)
{
    return (struct string){str, strlen(str)};
}

struct string
string_append(struct string const *const string, char const ch)
{

// Find space for one character in the string pool.

    char const *const location = string_pool_reserve(1);

// If the string isn't at the end of the pool, copy it.

    struct string result = *string;
    if (!result.begin || location != result.begin + result.size) {
        string_pool_reserve(1 + result.size);
        result.begin = string_pool_add(result.begin, result.size);
    }

    string_pool_add(&ch, 1);
    ++result.size;
    return result;
}

struct string
string_concat(struct string const *const lhs, struct string const *const rhs)
{
    if (0 == lhs->size) {
        return *rhs;
    }

    struct string result = *lhs;
    if (0 != rhs->size) {
        if (result.begin + result.size != rhs->begin) {

// If the strings aren't adjacent, concatenate them.

            char const *const location = string_pool_reserve(rhs->size);

// If the left string isn't at the end of the pool, copy it.

            if (location != result.begin + result.size) {
                string_pool_reserve(result.size + rhs->size);
                result.begin = string_pool_add(result.begin, result.size);
            }

            string_pool_add(rhs->begin, rhs->size);
        }
        result.size += rhs->size;
    }

    return result;
}
