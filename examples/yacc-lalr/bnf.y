%{

#include "dstring.h"
#include "lalr.h"
#include "rule.h"

#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <wctype.h>

extern void yyerror(char const*);
extern int yylex(void);

%}

%union {
    struct string string;
    struct rbody const *rbody;
    struct rbody_seq const *rbody_seq;
    struct rule_list const *rule_list;
}

%token <string> DIGIT INITIAL OTHER

%type <string> escaped_char id_char id_char_seq identifier literal literal_char symbol
%type <rbody> rbody
%type <rbody_seq> rbody_seq
%type <rule_list> rule rules

%%

program : ws_seq rules
        { lalr($2); }
        ;

rules : rule rules
      { $$ = rule_list_concat($1, $2); }

      | rule
      { $$ = $1; }

      ;

rule : identifier ws_seq ':' ws_seq rbody_seq
     { $$ = make_rule_list(&$1, $5); }
     ;

rbody_seq : rbody ';' ws_seq
          { $$ = rbody_seq_new($1, NULL); }

          | rbody '|' ws_seq rbody_seq
          { $$ = rbody_seq_new($1, $4); }

          ;

rbody : symbol ws ws_seq rbody
      { $$ = rbody_new(&$1, $4); }

      | symbol
      { $$ = rbody_new(&$1, NULL); }

      |
      { $$ = NULL; }

      ;

symbol : literal
       | identifier
       ;

literal : '\'' literal_char '\''
        { $$ = string_char('\'');
          $$ = string_concat(&$$, &$2);
          $$ = string_append(&$$, '\'');
        }
        ;

literal_char : DIGIT
             | INITIAL
             | OTHER
             | '\t'
             { $$ = string_str("\\t"); }

             | ' '
             { $$ = string_char(' '); }

             | ':'
             { $$ = string_char(':'); }

             | ';'
             { $$ = string_char(';'); }

             | '\\' escaped_char
             { $$ = string_char('\\'); $$ = string_concat(&$$, &$2); }

             | '|'
             { $$ = string_char('|'); }

             ;

escaped_char : INITIAL
             | OTHER
             | DIGIT DIGIT DIGIT
             { $$ = string_concat(&$1, &$2);
               $$ = string_concat(&$$, &$3);
             }

             | '\''
             { $$ = string_char('\''); }

             | '\\'
             { $$ = string_char('\\'); }

             ;

identifier : INITIAL id_char_seq
           { $$ = string_concat(&$1, &$2); }
           ;

id_char_seq : id_char_seq id_char
            { $$ = string_concat(&$1, &$2); }

            |
            { $$ = (struct string){0}; }

            ;

id_char : DIGIT
        | INITIAL
        ;

ws_seq : ws ws_seq
       |
       ;

ws : ' '
   | '\f'
   | '\n'
   | '\t'
   | '\v'
   ;

%%

int yylex()
{
    static mbstate_t mbstate;
    wchar_t wc;

// Read one (possibly multi-byte) character, storing the result
//      as a multi-byte character in yylval.string, and
//      as a wide character in wc.
//
// Feed individual bytes to mbrtowc() which updates mbstate and returns
// (size_t)-2 until a character is complete.

    yylval.string = (struct string){0};
    size_t r;
    while (
        !yylval.string.size ||
        (size_t)-2 == (r = mbrtowc(&wc, yylval.string.begin + yylval.string.size - 1, 1, &mbstate))
    ) {
        int const ch = getchar();
        if (EOF == ch) {
            return EOF;
        }
        yylval.string = string_append(&yylval.string, ch);
    }
    if (r == (size_t)-1) {
        perror("stdin");
        exit(1);
    }

    if (iswdigit(wc)) {
        return DIGIT;
    }

    if (iswalpha(wc) || '.' == wc || '_' == wc) {
        return INITIAL;
    }

// Return characters which appear as literals in the grammar as themselves.

    static char const themselves[] = "\f\n\t\v \':;\\|";
    if (CHAR_MIN <= wc && wc <= CHAR_MAX && strchr(themselves, wc)) {
        return wc;
    }

    return OTHER;
}
