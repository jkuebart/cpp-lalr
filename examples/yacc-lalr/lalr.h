#ifndef LALR_H
#define LALR_H

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus

struct rule_list;

/**
 * Generate and print an LALR(1) parsing table for the productions in the
 * rule list.
 *
 * @param rule_list A list of grammar productions.
 */
void lalr(struct rule_list const* rule_list);

#ifdef __cplusplus
} // extern "C"
#endif // __cplusplus

#endif // LALR_H
