#ifndef SUBSET_H
#define SUBSET_H

#include <algorithm>
#include <functional>
#include <map>
#include <ostream>
#include <set>
#include <string>
#include <tuple>
#include <vector>

namespace subset
{

/**
 * I represent a deterministic finite automaton whose states consist of
 * sets of states of a non-deterministic automaton.
 */

    template<typename State, typename Label>
    class Dfa
    {
    public:
        using Transitions = std::map<Label, std::size_t>;
        using Reductions = std::set<std::size_t>;
        using States = std::vector<std::tuple<State, Transitions, Reductions>>;
        using VisitStates = std::function<void(State const&)>;
        using VisitLabels = std::function<void(Label const&, State const&)>;

        explicit Dfa(States states)
        : mStates{std::move(states)}
        {}

        State const& start() const
        {
            return {};
        }

        void closure(State const& state, VisitStates& func) const
        {
            func(state);
        }

        void edges(State const& state, VisitLabels& func) const
        {
            auto const it{std::find_if(
                mStates.begin(),
                mStates.end(),
                [&state](auto const& tpl)
                {
                    return state == std::get<0>(tpl);
                }
            )};
            assert(mStates.end() != it);

            for (auto const& edge : std::get<1>(*it)) {
                assert(edge.second < mStates.size());
                func(edge.first, std::get<0>(mStates[edge.second]));
            }
        }

        friend std::ostream& operator <<(std::ostream& os, Dfa const& dfa)
        {
            std::size_t longestLabel{0};
            std::set<Label> labels{};
            for (auto const& state : dfa.mStates) {
                for (auto const& edge : std::get<1>(state)) {
                    auto const& label{std::get<0>(edge)};
                    labels.insert(label);
                    if (longestLabel < label.size()) {
                        longestLabel = label.size();
                    }
                }
            }

            std::string const whitespace(1 + longestLabel, ' ');
            os << whitespace;
            for (auto const& label : labels) {
                os << whitespace.substr(label.size()) << label;
            }
            os << "\n\n";

            for (std::size_t i{0}; i != dfa.mStates.size(); ++i)
            {
                os << whitespace.substr(i < 10 ? 1 : 2) << i;

// Show this state's transitions.

                auto const& transitions{std::get<1>(dfa.mStates[i])};
                for (auto const& label : labels) {
                    if (0 == transitions.count(label)) {
                        os << whitespace;
                    } else {
                        std::size_t const target{transitions.at(label)};
                        os <<
                            whitespace.substr(target < 10 ? 2 : 3) <<
                            's' << target;
                    }
                }

// If this state has any reductions, show them.

                auto const& reductions{std::get<2>(dfa.mStates[i])};
                for (auto const& reduction : reductions) {
                    os << '\n' << whitespace;
                    for (std::size_t j{0}; j != labels.size(); ++j) {
                        os <<
                            whitespace.substr(reduction < 10 ? 2 : 3) <<
                            'r' << reduction;
                    }
                }

                os << "\n\n";
            }

            return os;
        }

    private:
        States mStates;
    };

/**
 * Perform the subset algorithm on the non-deterministic automaton nfa
 * and return a deterministic automaton.
 *
 * @param nfa A non-deterministic finite automaton.
 * @return A deterministic finite automaton.
 */

    template<typename Nfa>
    auto createDfa(Nfa const& nfa)
    {
        using NfaState = typename Nfa::State;
        using Label = typename Nfa::Label;

        auto const start{nfa.start()};

// Create deterministic states from sets of non-deterministic states until
// no new such sets occur.

        using State = std::set<NfaState>;
        typename Dfa<State, Label>::States states{
            {
                {start.begin(), start.end()},
                {},
                {}
            }
        };
        std::size_t completeStates{0};
        while (completeStates != states.size()) {

// Find reductions and edges for the current state's closure.

            auto const& incompleteState{
                nfa.closure({
                    std::get<0>(states[completeStates]).begin(),
                    std::get<0>(states[completeStates]).end()
                })
            };

// Collect non-deterministic target states of the edges from any of the
// constituent non-deterministic states.

            std::map<Label, State> edges{};
            for (NfaState const& nfaState : incompleteState) {

// Note down the production indices of this state.

                auto const& rdctns{nfa.reductions(nfaState)};
                std::get<2>(states[completeStates]).insert(rdctns.begin(), rdctns.end());

// Build a map of transitions for this state.

                for (auto const& nfaEdge : nfa.edges(nfaState)) {
                    edges[std::get<0>(nfaEdge)].insert(std::get<1>(nfaEdge));
                }
            }

            for (auto const& edge : edges) {
                State const& newState{std::get<1>(edge)};
                auto const it{find_if(
                    states.begin(),
                    states.end(),
                    [&newState](auto const& state) {
                        return std::get<0>(state) == newState;
                    }
                )};

// If an edge's target state is new, add it to the list of states. In both
// cases, add the index of the target state to the incomplete state's list
// of target state indices.

                std::size_t const index{
                    static_cast<std::size_t>(it - states.begin())
                };
                assert(0 == std::get<1>(states[completeStates]).count(std::get<0>(edge)));
                std::get<1>(states[completeStates]).emplace(std::get<0>(edge), index);

                if (states.size() == index) {
                    states.push_back({newState, {}, {}});
                }
            }

            ++completeStates;
        }
        return Dfa{states};
    }
}

#endif // SUBSET_H
