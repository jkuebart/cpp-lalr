LALR(1) table generator
=======================

This tool reads a BNF grammar specification and builds an LALR(1) parsing
table from it. The table is then printed. This can be useful for diagnosing
shift/reduce and reduce/reduce conflicts in (small) grammars.

This code serves as a testbed for the table construction algorithm.
