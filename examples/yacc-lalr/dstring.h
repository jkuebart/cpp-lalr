#ifndef DSTRING_H
#define DSTRING_H

#include <stddef.h>

struct string
{
    char const *begin;
    size_t size;
};

/**
 * Create a single-character string.
 *
 * @param ch The character.
 * @return A string containing the single character.
 */
struct string string_char(char ch);

/**
 * Wrap a string literal.
 *
 * @note It is assumed that string literals have static storage duration
 * and need not be copied to the string pool.
 *
 * @param str A string literal.
 * @return The string literal wrapped as a string.
 */
struct string string_str(char const *str);

/**
 * Append one character to a string.
 *
 * @param string The string.
 * @param ch A character.
 * @return The string.
 */
struct string string_append(struct string const *string, char ch);

/**
 * Concatenate two strings.
 *
 * @param lhs The first string.
 * @param rhs The second string.
 * @return The concatenation of the two strings.
 */
struct string string_concat(struct string const *lhs, struct string const *rhs);

#endif // DSTRING_H
