#include <liblalr/parser.h>

#include <libmeta/meta.h>

#include <iostream>
#include <string_view>
#include <variant>
#include <vector>

namespace
{

// Terminals

    template<char Chr>
    struct Char {
        friend std::ostream& operator <<(std::ostream& os, Char const&)
        {
            return os << '\'' << Chr << '\'';
        }
    };

    struct Id {
        explicit Id(char id_)
        : id{id_}
        {}

        friend std::ostream& operator <<(std::ostream& os, Id const& rhs)
        {
            return os << "Id{" << rhs.id << '}';
        }

        char id;
    };

// AST

    struct Operation
    {
        virtual ~Operation() = default;

        virtual std::ostream& toStream(std::ostream&) const = 0;

        friend std::ostream& operator <<(std::ostream& os, Operation const& rhs)
        {
            return rhs.toStream(os);
        }
    };

    struct IdOperation: Operation
    {
        explicit IdOperation(Id&& id_)
        : id{std::move(id_)}
        {}

        std::ostream& toStream(std::ostream& os) const override
        {
            return os << id;
        }

        Id id;
    };

    struct BinaryOperation: Operation
    {
        BinaryOperation(char op_, std::unique_ptr<Operation> lhs_, std::unique_ptr<Operation> rhs_)
        : op{op_}
        , lhs{std::move(lhs_)}
        , rhs{std::move(rhs_)}
        {}

        std::ostream& toStream(std::ostream& os) const override
        {
            return os << '(' << op << ' ' << *lhs << ' ' << *rhs << ')';
        }

        char op;
        std::unique_ptr<Operation> lhs;
        std::unique_ptr<Operation> rhs;
    };

    struct CalcOp
    {
        std::unique_ptr<Operation> expr;
    };

// Nonterminals

    struct Factor: CalcOp
    {
        Factor(Char<'('>, CalcOp&& expr, Char<')'>)
        : CalcOp{std::move(expr)}
        {}

        explicit Factor(Id&& id)
        : CalcOp{std::make_unique<IdOperation>(std::move(id))}
        {}
    };

    struct Term: CalcOp
    {
        explicit Term(CalcOp&& factor)
        : CalcOp{std::move(factor.expr)}
        {}

        Term(CalcOp&& term, Char<'*'>, CalcOp&& factor)
        : CalcOp{std::make_unique<BinaryOperation>('*', std::move(term.expr), std::move(factor.expr))}
        {}
    };

    struct Expr: CalcOp
    {
        explicit Expr(CalcOp&& term)
        : CalcOp{std::move(term.expr)}
        {}

        Expr(CalcOp&& expr, Char<'+'>, CalcOp&& term)
        : CalcOp{std::make_unique<BinaryOperation>('+', std::move(expr.expr), std::move(term.expr))}
        {}
    };

    using lalr::Production;
    using Productions =
        meta::list<
            Production<Expr, Expr, Char<'+'>, Term>,
            Production<Expr, Term>,
            Production<Factor, Char<'('>, Expr, Char<')'>>,
            Production<Factor, Id>,
            Production<Term, Factor>,
            Production<Term, Term, Char<'*'>, Factor>
        >;

    int Main(std::vector<std::string_view>)
    {
        lalr::Parser<Productions, Expr> parser{};

        char c;
        while (std::cin >> c) {
            switch (c)
            {
            case '(':
                parser.token(Char<'('>{});
                break;
            case ')':
                parser.token(Char<')'>{});
                break;
            case '*':
                parser.token(Char<'*'>{});
                break;
            case '+':
                parser.token(Char<'+'>{});
                break;
            default:
                if (std::isalnum(c)) {
                    parser.token(Id{c});
                } else {
                    std::cerr << "Invalid token '" << c << "'\n";
                }
                break;
            }
        }

        if (parser.finish()) {
            std::cout << "accept " << *std::get<Expr>(parser.value()).expr << '\n';
        } else {
            std::cout << "reject\n";
        }

        return 0;
    }
} // namespace <anonymous>

int
main(int
c,char**v){return
Main({v,c+v});}
