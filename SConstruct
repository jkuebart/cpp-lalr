# Allow user to specify build mode.
vars = Variables()
vars.AddVariables(
    ListVariable(
        "MODES",
        "The build mode.",
        "release",
        ["debug", "release"]
    ),
)

env = Environment(tools=["default", RegressionTest], variables=vars)
Help(vars.GenerateHelpText(env), append=True)
if vars.UnknownVariables():
    print("Unknown command line variables: %s" % (vars.UnknownVariables()))
    Exit(64) # EX_USAGE

# Common configuration.
env.Append(
    CCFLAGS=["-Weverything", "-g"],
    CFLAGS=["-std=c99"],
    CPPPATH=["#liblalr", "#libmeta"],
    CXXFLAGS=["-Wno-c++98-compat", "-std=%s" % (ARGUMENTS.get("std", "c++17"),)],
)

# Set up configuration-specific environments.
envs = {config: env.Clone() for config in ("debug", "release")}

envs["debug"].Append(
    CCFLAGS=["-fsanitize=address,undefined", "-O0"],
    LINKFLAGS=["-fsanitize=address,undefined"],
)

envs["release"].Append(
    CCFLAGS=["-O"],
    CPPDEFINES=["NDEBUG"],
)

# Build each configuration.
for config, env in envs.items():
    if config not in env["MODES"]:
        continue

    env.SConscript(
        dirs=["."],
        duplicate=0,
        exports=["env"],
        variant_dir=".obj/$PLATFORM/%s" % (config,),
    )
