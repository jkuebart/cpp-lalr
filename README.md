Cpp-LALR(1)
===========

This project provides a typesafe compile-time parser generator for »modern«
C++. It allows the user to specify their grammar using C++ code. A parser
is constructed from the grammar at compile-time. Type-checking is performed
for the values of terminals and non-terminals in the grammar.

In contrast to existing tools, the result is a table-driven LR(1) parser
that avoids many of the problems faced by LL(1) parsers used by existing
tools such as [Boost.Spirit][SPIRIT], for example. Particularly, LR(1)
parsers

 1. don't require backtracking,
 2. typically resort to operator precedence parsing, and
 3. are prone to stack overflows given malicious inputs.

The idea is based on [yacc(1)][YACC], but eliminates the intervening
code-generation step by integrating seamlessly with C++.


Documentation
-------------

Some [slides][SLIDES] are available.


Building
--------

The source code can be built using [SCons][SCONS].

    scons

Files are built in subdirectories of `.obj`.

The build mode can be specified as `release` (the default) or `debug` on
the command line. Multiple build modes can be given as one or more `MODES`
arguments.

    scons MODES=debug

In order to clean the `.obj` directory, use

    scons -c MODES=all


[SCONS]: https://scons.org/
[SLIDES]: https://jkuebart.gitlab.io/cpp-lalr/
[SPIRIT]: http://boost-spirit.com/home/
[YACC]: http://pubs.opengroup.org/onlinepubs/9699919799/utilities/yacc.html
