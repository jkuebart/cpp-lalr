#include <liblalr/parse_table.h>

#include <array>
#include <string_view>
#include <type_traits>
#include <vector>

using namespace lalr;

namespace
{
    struct Expr;
    struct Factor;
    struct Id;
    struct Term;
    template<char>
    struct Char;

    template<std::size_t Dot, typename... Symbols>
    using MakeItem = Item<Dot, Production<Symbols...>>;

    using Productions =
        meta::list<
            Production<Expr, Expr, Char<'+'>, Term>,
            Production<Expr, Term>,
            Production<Factor, Char<'('>, Expr, Char<')'>>,
            Production<Factor, Id>,
            Production<Term, Factor>,
            Production<Term, Term, Char<'*'>, Factor>
        >;

    using State0Items = ToItemSet<Productions, Expr>;
    static_assert(std::is_same_v<
        meta::list<
            MakeItem<0, Expr, Expr, Char<'+'>, Term>,
            MakeItem<0, Expr, Term>
        >,
        State0Items
    >);
    using State0Closure = Closure<Productions, State0Items>;
    static_assert(std::is_same_v<
        meta::list<
            MakeItem<0, Term, Term, Char<'*'>, Factor>,
            MakeItem<0, Factor, Id>,
            MakeItem<0, Factor, Char<'('>, Expr, Char<')'>>,
            MakeItem<0, Term, Factor>,
            MakeItem<0, Expr, Term>,
            MakeItem<0, Expr, Expr, Char<'+'>, Term>
        >,
        State0Closure
    >);

    using State0Edges = OutgoingEdges<State0Closure>;
    static_assert(std::is_same_v<
        meta::list<
            meta::list<
                Id,
                MakeItem<1, Factor, Id>
            >,
            meta::list<
                Char<'('>,
                MakeItem<1, Factor, Char<'('>, Expr, Char<')'>>
            >,
            meta::list<
                Factor,
                MakeItem<1, Term, Factor>
            >,
            meta::list<
                Term,
                MakeItem<1, Term, Term, Char<'*'>, Factor>,
                MakeItem<1, Expr, Term>
            >,
            meta::list<
                Expr,
                MakeItem<1, Expr, Expr, Char<'+'>, Term>
            >
        >,
        State0Edges
    >);

/**
 * Replace known states by their canonical form and add new states to the
 * state map.
 */

    using EdgesAndStates1 = Canonicalise<meta::list<State0Items>, State0Closure>;

    static_assert(std::is_same_v<
        meta::list<

// Edges

            meta::list<
                meta::list<
                    Id,
                    MakeItem<1, Factor, Id>
                >,
                meta::list<
                    Char<'('>,
                    MakeItem<1, Factor, Char<'('>, Expr, Char<')'>>
                >,
                meta::list<
                    Factor,
                    MakeItem<1, Term, Factor>
                >,
                meta::list<
                    Term,
                    MakeItem<1, Term, Term, Char<'*'>, Factor>,
                    MakeItem<1, Expr, Term>
                >,
                meta::list<
                    Expr,
                    MakeItem<1, Expr, Expr, Char<'+'>, Term>
                >
            >,

// States

            meta::list<
                MakeItem<1, Factor, Id>
            >,
            meta::list<
                MakeItem<1, Factor, Char<'('>, Expr, Char<')'>>
            >,
            meta::list<
                MakeItem<1, Term, Factor>
            >,
            meta::list<
                MakeItem<1, Term, Term, Char<'*'>, Factor>,
                MakeItem<1, Expr, Term>
            >,
            meta::list<
                MakeItem<1, Expr, Expr, Char<'+'>, Term>
            >

        >,

        EdgesAndStates1
    >);

/**
 * Augment the grammar by a trivial production for its starting symbol.
 * The parser accepts the input if and only if it reduces by this
 * production.
 */

    using StateMap = BuildStateMap<Productions, Expr>;

    static_assert(12 == StateMap::size());
    static_assert(std::is_same_v<
        meta::list<
            meta::list<
                meta::list<MakeItem<1, Expr, Expr, Char<'+'>, Term>, MakeItem<1, StartSymbol, Expr> >,
                meta::list<Char<'+'>, MakeItem<2, Expr, Expr, Char<'+'>, Term> > >,

            meta::list<
                meta::list<MakeItem<1, Term, Term, Char<'*'>, Factor>, MakeItem<1, Expr, Term> >,
                meta::list<Char<'*'>, MakeItem<2, Term, Term, Char<'*'>, Factor> > >,

            meta::list<
                meta::list<MakeItem<1, Term, Factor> > >,

            meta::list<
                meta::list<MakeItem<3, Term, Term, Char<'*'>, Factor> > >,

            meta::list<
                meta::list<MakeItem<2, Term, Term, Char<'*'>, Factor> >,
                meta::list<Id, MakeItem<1, Factor, Id> >,
                meta::list<Char<'('>, MakeItem<1, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Factor, MakeItem<3, Term, Term, Char<'*'>, Factor> > >,

            meta::list<
                meta::list<MakeItem<1, Term, Term, Char<'*'>, Factor>, MakeItem<3, Expr, Expr, Char<'+'>, Term> >,
                meta::list<Char<'*'>, MakeItem<2, Term, Term, Char<'*'>, Factor> > >,

            meta::list<
                meta::list<MakeItem<2, Expr, Expr, Char<'+'>, Term> >,
                meta::list<Id, MakeItem<1, Factor, Id> >,
                meta::list<Char<'('>, MakeItem<1, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Factor, MakeItem<1, Term, Factor> >,
                meta::list<Term, MakeItem<1, Term, Term, Char<'*'>, Factor>, MakeItem<3, Expr, Expr, Char<'+'>, Term> > >,

            meta::list<
                meta::list<MakeItem<3, Factor, Char<'('>, Expr, Char<')'> > > >,

            meta::list<
                meta::list<MakeItem<1, Expr, Expr, Char<'+'>, Term>, MakeItem<2, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Char<')'>, MakeItem<3, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Char<'+'>, MakeItem<2, Expr, Expr, Char<'+'>, Term> > >,

            meta::list<
                meta::list<MakeItem<1, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Id, MakeItem<1, Factor, Id> >,
                meta::list<Char<'('>, MakeItem<1, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Factor, MakeItem<1, Term, Factor> >,
                meta::list<Term, MakeItem<1, Term, Term, Char<'*'>, Factor>, MakeItem<1, Expr, Term> >,
                meta::list<Expr, MakeItem<1, Expr, Expr, Char<'+'>, Term>, MakeItem<2, Factor, Char<'('>, Expr, Char<')'> > > >,

            meta::list<
                meta::list<MakeItem<1, Factor, Id> > >,

            meta::list<
                meta::list<MakeItem<0, StartSymbol, Expr> >,
                meta::list<Id, MakeItem<1, Factor, Id> >,
                meta::list<Char<'('>, MakeItem<1, Factor, Char<'('>, Expr, Char<')'> > >,
                meta::list<Factor, MakeItem<1, Term, Factor> >,
                meta::list<Term, MakeItem<1, Term, Term, Char<'*'>, Factor>, MakeItem<1, Expr, Term> >,
                meta::list<Expr, MakeItem<1, Expr, Expr, Char<'+'>, Term>, MakeItem<1, StartSymbol, Expr> > > >,

        StateMap
    >);

/**
 * Frequently, we have to lookup the indexes of the target states.
 */

    using IndexedStateMap = BuildIndexedTable<Productions, Expr>;
    static_assert(std::is_same_v<
        meta::list<
            meta::list<
                meta::list<Char<'+'>, meta::size_t<6> > >,
            meta::list<
                meta::list<Char<'*'>, meta::size_t<4> > >,
            meta::list<>,
            meta::list<>,
            meta::list<
                meta::list<Id, meta::size_t<10> >,
                meta::list<Char<'('>, meta::size_t<9> >,
                meta::list<Factor, meta::size_t<3> > >,
            meta::list<
                meta::list<Char<'*'>, meta::size_t<4> > >,
            meta::list<
                meta::list<Id, meta::size_t<10> >,
                meta::list<Char<'('>, meta::size_t<9> >,
                meta::list<Factor, meta::size_t<2> >,
                meta::list<Term, meta::size_t<5> > >,
            meta::list<>,
            meta::list<
                meta::list<Char<')'>, meta::size_t<7> >,
                meta::list<Char<'+'>, meta::size_t<6> > >,
            meta::list<
                meta::list<Id, meta::size_t<10> >,
                meta::list<Char<'('>, meta::size_t<9> >,
                meta::list<Factor, meta::size_t<2> >,
                meta::list<Term, meta::size_t<1> >,
                meta::list<Expr, meta::size_t<8> > >,
            meta::list<>,
            meta::list<
                meta::list<Id, meta::size_t<10> >,
                meta::list<Char<'('>, meta::size_t<9> >,
                meta::list<Factor, meta::size_t<2> >,
                meta::list<Term, meta::size_t<1> >,
                meta::list<Expr, meta::size_t<0> > > >,

        IndexedStateMap
    >);

    class DummyParser
    {
    public:
#ifndef NDEBUG
        template<typename Production, std::size_t N>
        void reduce(std::array<std::size_t, N> const&)
        {
        }
#endif // NDEBUG
    };

    using CalcTable = BuildParseTable<Productions, Expr>;
    static_assert(6 == CalcTable::shift<Char<'+'>>[0]);
    static_assert(4 == CalcTable::shift<Char<'*'>>[1]);
    static_assert(9 == CalcTable::shift<Char<'('>>[4]);
    static_assert(3 == CalcTable::shift<Factor>[4]);

    int Main(std::vector<std::string_view>)
    {
#ifndef NDEBUG
        DummyParser dp{};
#endif // NDEBUG

        assert((ActionResult::Accept == CalcTable::reduce<DummyParser>[0](dp)));
        assert((ActionResult::Reduce == CalcTable::reduce<DummyParser>[1](dp)));
        assert((ActionResult::Error == CalcTable::reduce<DummyParser>[4](dp)));

        return 0;
    }
}

int
main(int
c,char**v){return
Main({v,c+v});}
