#ifndef LIBLALR_PARSER_H
#define LIBLALR_PARSER_H

#include "parse_table.h"

#include <libmeta/meta.h>

#include <array>
#include <iostream>
#include <limits>
#include <utility>
#include <variant>
#include <vector>

namespace lalr
{
    template<
        typename Production,
        typename Indexes = std::make_index_sequence<Production::Rhs::size()>
    >
    struct Reduce;

    template<typename Nonterminal, typename... Symbols, std::size_t... Is>
    struct Reduce<
        Production<Nonterminal, Symbols...>,
        std::index_sequence<Is...>
    >
    {
        template<typename Values>
        static constexpr void apply(Values& values)
        {
            if constexpr (0 == sizeof...(Symbols)) {
                return values.push_back(Nonterminal{});
            } else {
                std::size_t const firstIndex{values.size() - sizeof...(Symbols)};

// Replace the last N stack values with a newly constructed non-terminal.

                values[firstIndex] =
                    Nonterminal{
                        std::move(std::get<Symbols>(values[firstIndex + Is]))...
                    };

                while (1 + firstIndex < values.size()) {
                    values.pop_back();
                }
            }
        }
    };

    template<typename Productions, typename StartSymbol>
    class Parser
    {
    public:
        using Value =
            meta::apply<
                meta::quote<std::variant>,
                meta::unique<meta::join<
                    meta::transform<
                        Productions,
                        meta::quote<lalr::ProductionToList>
                    >
                >>
            >;

        using ParseTable = BuildParseTable<Productions, StartSymbol>;

        Parser()
        : mStates{ParseTable::startState()}
        , mValues{}
        {}

        Value const& value() const
        {
            return mValues.back();
        }

        template<typename Token>
        bool token(Token const& tkn)
        {

// Reduce until we can shift the given token.

            std::size_t next{ParseTable::template shift<Token>[mStates.back()]};
            while (std::numeric_limits<std::size_t>::max() == next) {
                if (ActionResult::Reduce != ParseTable::template reduce<Parser>[mStates.back()](*this)) {
                    std::cerr <<
                        "Reduce error: tkn=" << tkn <<
                        " state=" << mStates.back() <<
                        '\n';
                    return false;
                }
                next = ParseTable::template shift<Token>[mStates.back()];
            }

// Shift the given token.

            mStates.push_back(next);
            mValues.push_back(tkn);
            return true;
        }

/**
 * Called at the end of the input.
 *
 * @return Whether the parser accepts the input.
 */

        bool finish()
        {
            for (;;) {
                switch (ParseTable::template reduce<Parser>[mStates.back()](*this)) {
                case ActionResult::Accept:
                    return true;

                case ActionResult::Error:
                    std::cerr <<
                        "Reduce error: state=" << mStates.back() <<
                        '\n';
                    return false;

                case ActionResult::Reduce:
                    continue;
                }
            }

            return false;
        }

        template<typename Production, std::size_t N>
        void reduce(std::array<std::size_t, N> const& gotos)
        {
            assert(Production::Rhs::size() <= mValues.size());
            assert(mStates.size() == 1 + mValues.size());

// Replace the last N stack values with a newly constructed non-terminal.

            Reduce<Production>::apply(mValues);

// Replace the last N stack states with the target for this non-terminal.

            mStates.resize(mValues.size());
            assert(std::numeric_limits<std::size_t>::max() != gotos[mStates.back()]);
            mStates.push_back(gotos[mStates.back()]);
        }

    private:
        std::vector<std::size_t> mStates;
        std::vector<Value> mValues;
    };
}

#endif // LIBLALR_PARSER_H
