#ifndef LIBLALR_PARSE_TABLE_H
#define LIBLALR_PARSE_TABLE_H

#include <libmeta/meta.h>
#include <libmeta/metamap.h>

#include <array>
#include <limits>
#include <type_traits>

namespace lalr
{

/**
 * A production is represented by the non-terminal on its left-hand side
 * and a list of symbols on the right-hand side.
 */

    template<typename NonTerm, typename... Symbols>
    struct Production
    {
        using Nonterminal = NonTerm;
        using Rhs = meta::list<Symbols...>;
    };

/*
 * Utility functions.
 */

    template<typename Symbol>
    struct ForSymbol
    {
        template<typename Production>
        using invoke = std::is_same<Symbol, typename Production::Nonterminal>;
    };

/**
 * Add an item to a map, concatenating it to a previously existing entry
 * with the same key.
 */

    template<typename T>
    struct MapAppendHelper
    {
        template<typename E>
        using invoke = meta::concat<T, meta::pop_front<E>>;
    };

    template<typename M, typename T>
    using MapAppend = metamap::update<M, T, MapAppendHelper<T>>;

/**
 * Build a map from a list of key-value pairs.
 *
 * Values with identical keys are concatenated, so the list [[K0, V0], [K1,
 * V1], [K0, V2]] becomes the map [[K0, V0, V2], [K1, V1]].
 */

    template<typename L>
    using BuildMap = meta::reverse_fold<L, meta::list<>, meta::quote<MapAppend>>;

/**
 * Compare two sets irrespective of the order of their elements.
 */

    template<typename S, typename T, bool = (S::size() == T::size())>
    struct SetEquals: std::false_type
    {};

    template<typename S, typename... Ts>
    struct SetEquals<S, meta::list<Ts...>, true>: std::conjunction<meta::contains<S, Ts>...>
    {};

/**
 * States are represented by »item sets« where each item is a grammar
 * production and a position (»dot«).
 *
 * The position is stored as an index into the production P, such that
 * P::dot <= P::Production::Rhs::size() and when
 * P::dot < P::Production::Rhs::size(), then at<P::Production::Rhs, P::dot>
 * yields the token to the right of the dot.
 */

    template<std::size_t Dot, typename Prod>
    struct Item
    {
        static constexpr std::size_t dot{Dot};
        using Production = Prod;
    };

    template<typename Production>
    using ProductionToItem = Item<0, Production>;

    template<typename Production>
    using ProductionToList =
        meta::push_front<
            typename Production::Rhs,
            typename Production::Nonterminal
        >;

    template<typename Item>
    using ItemToProduction = typename Item::Production;

    template<typename Item>
    using ItemSymbol = meta::at_c<typename Item::Production::Rhs, Item::dot>;

/**
 * Determine wheter an item's dot is at the end.
 */

    template<typename Item>
    using EndDot = std::bool_constant<Item::dot == Item::Production::Rhs::size()>;

/**
 * Return an item with the dot in the initial position for each production
 * starting with the given symbol.
 */

    template<typename Productions, typename Symbol>
    using ToItemSet =
        meta::transform<
            meta::filter<Productions, ForSymbol<Symbol>>,
            meta::quote<ProductionToItem>
        >;

/**
 * Produce a set of non-kernel items for the current symbol of the given
 * item.
 */

    template<typename Productions, typename Item, bool = EndDot<Item>::value>
    struct CompleteImpl
    {
        using type = ToItemSet<Productions, ItemSymbol<Item>>;
    };

    template<typename Productions, typename Item>
    struct CompleteImpl<Productions, Item, true>
    {
        using type = meta::list<>;
    };

    template<typename Productions, typename Item>
    using Complete = meta::e<CompleteImpl<Productions, Item>>;

/**
 * Return the closure of the given set of kernel items.
 */

    template<typename Productions, typename NewItems, typename Items>
    struct ClosureImpl;

    template<typename Productions, typename Items>
    struct ClosureImpl<Productions, meta::list<>, Items>
    {
        using type = Items;
    };

    template<
        typename Productions,
        typename NewItems,
        typename Items,
        bool = meta::contains<Items, meta::front<NewItems>>::value
    >
    struct ClosureHelper;

    template<typename Productions, typename NewItem, typename... NewItems, typename Items>
    struct ClosureHelper<Productions, meta::list<NewItem, NewItems...>, Items, true>
    {
        using type = meta::e<ClosureImpl<Productions, meta::list<NewItems...>, Items>>;
    };

    template<typename Productions, typename NewItem, typename... NewItems, typename Items>
    struct ClosureHelper<Productions, meta::list<NewItem, NewItems...>, Items, false>
    {
        using type =
            meta::e<ClosureImpl<

                Productions,

// Add the completion to the unprocessed items.

                meta::push_back<
                    Complete<Productions, NewItem>,
                    NewItems...
                >,

// Add the new item to the known items.

                meta::push_front<
                    Items,
                    NewItem
                >

            >>;
    };

    template<typename Productions, typename NewItems, typename Items>
    struct ClosureImpl
    {
        using type = meta::e<ClosureHelper<Productions, NewItems, Items>>;
    };

    template<typename Productions, typename ItemSet>
    using Closure = meta::e<ClosureImpl<Productions, ItemSet, meta::list<>>>;

/**
 * Assuming that an item's dot isn't at the end, return an outgoing edge.
 * Edges are represented by their label followed by the target item set.
 */

    template<typename InnerItem>
    using OutgoingEdge =
        meta::list<
            ItemSymbol<InnerItem>,
            Item<1 + InnerItem::dot, typename InnerItem::Production>
        >;

/**
 * Given a closed item set, compute outgoing edges as a map of symbols to
 * target item sets. Only items whose dots are not at the end possess an
 * outgoing edge.
 */

    template<typename ClosedItemSet>
    using OutgoingEdges =
        BuildMap<
            meta::transform<
                meta::filter<ClosedItemSet, meta::not_fn<meta::quote<EndDot>>>,
                meta::quote<OutgoingEdge>
            >
        >;

    template<typename ItemSet>
    struct ItemSetCompare
    {
        template<typename Set>
        using invoke = SetEquals<ItemSet, Set>;
    };

/**
 * If the edge's target item set is in the list of states, replace it by
 * its canonical form. Add the possibly updated edge to the list of edges.
 * If the edge's target item set is new, add it to the list of new states.
 * Return the new list of edges followed by the new states.
 */

    template<typename States>
    struct CanonicaliseHelper
    {
        template<typename EdgesAndStates, typename Edge>
        struct impl;

        template<typename Edges, typename... NewStates, typename Symbol, typename... Items>
        struct impl<meta::list<Edges, NewStates...>, meta::list<Symbol, Items...>>
        {
            using newItemSet = meta::list<Items...>;

// The canonical form of the new item set, or an empty list.

            using canonicalItemSet =
                meta::find_if<
                    meta::push_front<States, NewStates...>,
                    ItemSetCompare<newItemSet>
                >;

// If the edge's target item set is known, canonicalise it.

            using canonicalEdge =
                meta::push_front<
                    meta::front<meta::push_back<canonicalItemSet, newItemSet>>,
                    Symbol
                >;

            using type =
                meta::push_front<

// Add the target item set to the list of states if not yet known.

                    std::conditional_t<
                        0 == canonicalItemSet::size(),
                        meta::list<newItemSet, NewStates...>,
                        meta::list<NewStates...>
                    >,

// Add the new edge.

                    meta::push_front<
                        Edges,
                        canonicalEdge
                    >

                >;
        };

        template<typename EdgesAndStates, typename Edge>
        using invoke = meta::e<impl<EdgesAndStates, Edge>>;
    };


/**
 * Take a list of known states and a new state closure and compute a list
 * of the new state's outgoing edges followed by new states. The edges'
 * target states are canonicalised to the given list of previously known
 * states.
 */

    template<typename States, typename ClosedState>
    using Canonicalise =
        meta::reverse_fold<
            OutgoingEdges<ClosedState>,
            meta::list<meta::list<>>,
            CanonicaliseHelper<States>
        >;


/**
 * Replace known states by their canonical form and add new states to the
 * state map.
 *
 * State map entries consist of the state's set of kernel items followed by
 * the state's outgoing edges.
 */

    template<typename Productions, typename NewStates, typename... MapEntry>
    struct BuildStateMapImpl;

    template<typename Productions, typename... MapEntry>
    struct BuildStateMapImpl<Productions, meta::list<>, MapEntry...>
    {
        using type = meta::list<MapEntry...>;
    };

    template<typename Productions, typename NewState, typename... NewStates, typename... MapEntry>
    struct BuildStateMapImpl<Productions, meta::list<NewState, NewStates...>, MapEntry...>
    {

// Canonicalise takes a list of known states and a new state and returns a
// list of canonicalised outgoing edges followed by new states.

        using edgesAndStates =
            Canonicalise<
                meta::list<NewState, NewStates..., meta::front<MapEntry>...>,
                Closure<Productions, NewState>
            >;

        using type =
            meta::e<BuildStateMapImpl<

                Productions,

// Add the new states to the unprocessed states.

                meta::push_back<
                    meta::pop_front<edgesAndStates>,
                    NewStates...
                >,

// Add the new state and its outgoing edges to the state map.

                meta::push_front<
                    meta::front<edgesAndStates>,
                    NewState
                >,

                MapEntry...

            >>;
    };

/**
 * Augment the grammar by a trivial production for its starting symbol.
 * The parser accepts the input if and only if it reduces by this
 * production.
 */

    struct StartSymbol;
    template<typename Productions, typename Symbol>
    using BuildStateMap =
        meta::e<
            BuildStateMapImpl<
                Productions,
                meta::list<meta::list<Item<0, Production<StartSymbol, Symbol>>>>
            >
        >;

/**
 * Given a list of item sets and a list of edges where targets are item
 * sets, produce a list of edges where targets are the indices of item
 * sets.
 */

    template<typename States>
    struct Indicise
    {
        template<typename Edge>
        using Helper =
            meta::list<
                meta::front<Edge>,
                meta::find_index<States, meta::pop_front<Edge>>
            >;

// Replace edge's target states by their indices.

        template<typename Edges>
        using invoke = meta::transform<Edges, meta::quote<Helper>>;
    };

    template<typename StateMap>
    struct BuildIndexedTableImpl;

    template<typename... MapEntry>
    struct BuildIndexedTableImpl<meta::list<MapEntry...>>
    {
        using type =
            meta::transform<
                meta::list<meta::pop_front<MapEntry>...>,
                Indicise<meta::list<meta::front<MapEntry>...>>
            >;
    };

    template<typename Productions, typename StartSymbol>
    using BuildIndexedTable = meta::e<BuildIndexedTableImpl<BuildStateMap<Productions, StartSymbol>>>;

/**
 * Extract reducible productions from items with dots in end position for
 * each state in the state map.
 */

    template<typename Productions, typename StateMap>
    struct BuildReduceTableImpl;

    template<typename Productions, typename... MapEntry>
    struct BuildReduceTableImpl<Productions, meta::list<MapEntry...>>
    {
        using type =
            meta::list<
                meta::transform<
                    meta::filter<
                        Closure<Productions, meta::front<MapEntry>>,
                        meta::quote<EndDot>
                    >,
                    meta::quote<ItemToProduction>
                >...
            >;
    };

    template<typename Productions, typename StartSymbol>
    using BuildReduceTable =
        meta::e<BuildReduceTableImpl<Productions, BuildStateMap<Productions, StartSymbol>>>;

    enum class ActionResult
    {
        Accept,
        Error,
        Reduce
    };

/**
 * Given a state index and a grammar symbol, `shift` contains the target
 * state index. Given a production and a parser, `reduce` inovkes reduce().
 */

    template<typename IndexedStateMap, typename Productions>
    struct BuildParseTableImpl;

    template<typename... Edges, typename... Productions>
    struct BuildParseTableImpl<meta::list<Edges...>, meta::list<Productions...>>
    {
        static constexpr std::size_t startState()
        {
            return sizeof...(Edges) - 1;
        }

        template<typename Symbol>
        static constexpr std::array<std::size_t, sizeof...(Edges)> shift{
            meta::second<
                metamap::find<
                    Edges,
                    Symbol,
                    meta::list<Symbol, meta::size_t<std::numeric_limits<std::size_t>::max()>>
                >
            >::value...
        };

        template<typename Parser, typename Reductions>
        static constexpr ActionResult helper(Parser& parser)
        {
            if constexpr (0 == Reductions::size()) {
                return ActionResult::Error;
            } else {
                using Nonterminal = typename meta::front<Reductions>::Nonterminal;
                if constexpr (std::is_same_v<StartSymbol, Nonterminal>) {
                    return ActionResult::Accept;
                } else {
                    parser.template reduce<meta::front<Reductions>>(shift<Nonterminal>);
                    return ActionResult::Reduce;
                }
            }
        }

        template<typename Parser>
        static constexpr std::array<
            ActionResult(*)(Parser&),
            sizeof...(Productions)
        > reduce{
            helper<Parser, Productions>...
        };
    };

/**
 * Build an array of functions that reduce by each state's first
 * production.
 */

    template<typename Productions, typename StartSymbol>
    struct BuildParseTable
        : BuildParseTableImpl<
            BuildIndexedTable<Productions, StartSymbol>,
            BuildReduceTable<Productions, StartSymbol>
        >
    {};
}

#endif // LIBLALR_PARSE_TABLE_H
