#ifndef LIBMETA_METAMAP_H
#define LIBMETA_METAMAP_H

namespace metamap
{


/*
 * Map handling
 * ------------
 *
 * find, contains, insert, update.
 */

    template<typename K, typename, typename... Ms>
    meta::list<K, Ms...> find_helper(meta::list<K, Ms...>*);

    template<typename, typename D>
    D find_helper(...);

    template<typename M, typename K, typename D = void>
    using find = decltype(find_helper<K, D>(static_cast<meta::inherit<M>*>(nullptr)));

    template<typename M, typename K>
    inline constexpr bool contains{!std::is_same_v<void, find<M, K>>};

    template<typename M, typename T>
    using insert =
        std::conditional_t<
            contains<M, meta::front<T>>,
            M,
            meta::push_front<M, T>
        >;

    template<typename K, typename U>
    struct update_helper
    {
        template<typename T>
        struct impl
        {
            using type = T;
        };

        template<typename... Ts>
        struct impl<meta::list<K, Ts...>>
        {
            using type = meta::invoke<U, meta::list<K, Ts...>>;
        };

        template<typename T>
        using invoke = typename impl<T>::type;
    };

    template<typename M, typename T, typename U, bool>
    struct update_impl
    {
        using type = meta::transform<M, update_helper<meta::front<T>, U>>;
    };

    template<typename M, typename T, typename U>
    struct update_impl<M, T, U, false>
    {
        using type = meta::push_front<M, T>;
    };

    template<typename M, typename T, typename U = meta::id<T>>
    using update = typename update_impl<M, T, U, contains<M, meta::front<T>>>::type;
}

#endif // LIBMETA_METAMAP_H
