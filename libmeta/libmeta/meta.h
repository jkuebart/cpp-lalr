/*
 * This is heavily based on (and intended to be compatible to) Eric
 * Niebler's Meta library, see https://github.com/ericniebler/meta/.
 */

#ifndef LIBMETA_META_H
#define LIBMETA_META_H

#include <cstddef>
#include <limits>
#include <type_traits>
#include <utility>

namespace meta
{


/*
 * Basics
 * ------
 *
 * id, e, quote, invoke, defer, list, ptr, size_t.
 */

    template<typename T>
    struct id
    {
        using type = T;

        template<typename...>
        using invoke = T;
    };

    template<typename T>
    using e = typename T::type;

    template<template <typename...> typename F, typename... As>
    struct defer
    {
        using type = F<As...>;
    };

    template<template<typename...> typename F>
    struct quote
    {
        // Work around http://open-std.org/jtc1/sc22/wg21/docs/cwg_active.html#1430.
        template<typename... As>
        using invoke = e<defer<F, As...>>;
    };

    template<typename F, typename... Args>
    using invoke = typename F::template invoke<Args...>;

    template<typename... Es>
    struct list
    {
        static constexpr std::size_t size() noexcept
        {
            return sizeof...(Es);
        }
    };

    template<typename T>
    T* ptr;

    template<std::size_t N>
    using size_t = std::integral_constant<std::size_t, N>;


/*
 * Invocation
 * ----------
 *
 * not_fn.
 */

    template<typename F>
    struct not_fn
    {
        template<typename... As>
        using invoke = std::bool_constant<!invoke<F, As...>::value>;
    };

/*
 * Compatibility
 * -------------
 *
 * apply.
 */

    template<typename F, typename L>
    struct apply_impl;

    template<typename F, template<typename...> typename L, typename... A>
    struct apply_impl<F, L<A...>>
    {
        using type = invoke<F, A...>;
    };

    template<typename F, typename T, T... Ts>
    struct apply_impl<F, std::integer_sequence<T, Ts...>>
    {
        using type = invoke<F, std::integral_constant<T, Ts>...>;
    };

    template<typename F, typename L>
    using apply = e<apply_impl<F, L>>;


/*
 * List handling
 * -------------
 *
 * front, pop_front, second, concat, join, repeat_n, transform, at,
 * push_front, push_back, reverse_fold, filter, find_index, find_if.
 */

    template<typename>
    struct front_impl;

    template<typename E0, typename... Es>
    struct front_impl<list<E0, Es...>>
    {
        using type = E0;
    };

    template<typename L>
    using front = e<front_impl<L>>;

    template<typename>
    struct pop_front_impl;

    template<typename E0, typename... Es>
    struct pop_front_impl<list<E0, Es...>>
    {
        using type = list<Es...>;
    };

    template<typename L>
    using pop_front = e<pop_front_impl<L>>;

    template<typename L>
    using second = front<pop_front<L>>;

    template<typename...>
    struct concat_impl;

    template<>
    struct concat_impl<>: id<list<>>
    {};

    template<typename L>
    struct concat_impl<L>
    {
        using type = L;
    };

    template<typename... E0s, typename... E1s>
    struct concat_impl<list<E0s...>, list<E1s...>>
    {
        using type = list<E0s..., E1s...>;
    };

    template<typename... E0s, typename... E1s, typename... E2s, typename... Lists>
    struct concat_impl<list<E0s...>, list<E1s...>, list<E2s...>, Lists...>
        : concat_impl<list<E0s..., E1s..., E2s...>, Lists...>
    {};

    template<typename... Lists>
    using concat = e<concat_impl<Lists...>>;

    template<typename L>
    using join = apply<quote<concat>, L>;

    template<std::size_t, typename T>
    using repeat_n_helper = T;

    template<typename Is, typename T>
    struct repeat_n_impl;

    template<std::size_t... Is, typename T>
    struct repeat_n_impl<std::index_sequence<Is...>, T>
    {
        using type = list<repeat_n_helper<Is, T>...>;
    };

    template<std::size_t N, typename T = void>
    using repeat_n_c = e<repeat_n_impl<std::make_index_sequence<N>, T>>;

    template<typename L, typename F>
    struct transform_impl;

    template<typename... Es, typename F>
    struct transform_impl<list<Es...>, F>
    {
        using type = list<invoke<F, Es>...>;
    };

    template<typename L, typename F>
    using transform = e<transform_impl<L, F>>;

    template<typename>
    struct at_helper
    {};

    template<typename... Ts>
    struct at_helper<list<Ts...>>
    {
        template<typename T>
        static T eval(Ts*..., T*, ...);
    };

    template<typename, std::size_t>
    struct at_impl;

    template<typename... Es, std::size_t N>
    struct at_impl<list<Es...>, N>
        : decltype(at_helper<repeat_n_c<N>>::eval(ptr<id<Es>>...))
    {};

    template<typename L, std::size_t N>
    using at_c = e<at_impl<L, N>>;

    template<typename L, typename N>
    using at = at_c<L, N::value>;

    template<typename L, typename... E>
    struct push_front_impl;

    template<typename... Es, typename... E>
    struct push_front_impl<list<Es...>, E...>
    {
        using type = list<E..., Es...>;
    };

    template<typename L, typename... E>
    using push_front = e<push_front_impl<L, E...>>;

    template<typename L, typename... E>
    struct push_back_impl;

    template<typename... Es, typename... E>
    struct push_back_impl<list<Es...>, E...>
    {
        using type = list<Es..., E...>;
    };

    template<typename L, typename... E>
    using push_back = e<push_back_impl<L, E...>>;

    template<typename L, typename A0, typename F>
    struct reverse_fold_impl;

    template<typename A0, typename F>
    struct reverse_fold_impl<list<>, A0, F>
    {
        using type = A0;
    };

    template<typename E0, typename... Es, typename A0, typename F>
    struct reverse_fold_impl<list<E0, Es...>, A0, F>
    {
        using type = invoke<F, e<reverse_fold_impl<list<Es...>, A0, F>>, E0>;
    };

    template<typename L, typename A0, typename F>
    using reverse_fold = e<reverse_fold_impl<L, A0, F>>;

    template<typename P>
    struct filter_helper
    {
        template<typename E>
        using invoke =
            std::conditional_t<
                invoke<P, E>::value,
                list<E>,
                list<>
            >;
    };

    template<typename L, typename P>
    using filter = join<transform<L, filter_helper<P>>>;

    template<typename L, typename T>
    struct find_index_impl;

    template<typename T>
    struct find_index_impl<meta::list<>, T>
    {
        using type = size_t<std::numeric_limits<std::size_t>::max()>;
    };

    template<typename... Es, typename T>
    struct find_index_impl<meta::list<Es...>, T>
    {
        static constexpr std::size_t index() noexcept
        {
            constexpr bool b[]{std::is_same_v<Es, T>...};
            for (std::size_t i{0}; i != sizeof...(Es); ++i)
            {
                if (b[i]) {
                    return i;
                }
            }
            return std::numeric_limits<std::size_t>::max();
        }
        using type = size_t<index()>;
    };

    template<typename L, typename T>
    using find_index = e<find_index_impl<L, T>>;

    template<typename L, typename P>
    struct find_if_impl;

    template<typename P>
    struct find_if_impl<list<>, P>: id<list<>>
    {};

    template<typename E0, typename... Es, typename P>
    struct find_if_impl<list<E0, Es...>, P>
        : std::conditional<
            invoke<P, E0>::value,
            list<E0, Es...>,
            e<find_if_impl<list<Es...>, P>>
        >
    {};

    template<typename L, typename P>
    using find_if = e<find_if_impl<L, P>>;


/*
 * Set handling
 * ------------
 *
 * inherit, contains, insert, unique.
 */

    template<typename Set>
    struct inherit;

    template<typename... Ss>
    struct inherit<list<Ss...>>: Ss...
    {};

    template<typename Set>
    struct contains_impl;

    template<typename... Ss>
    struct contains_impl<list<Ss...>>: id<Ss>...
    {};

    template<typename Set, typename E>
    using contains = std::is_base_of<id<E>, contains_impl<Set>>;

    template<typename Set, typename E>
    using insert =
        std::conditional_t<
            contains<Set, E>::value,
            Set,
            push_front<Set, E>
        >;

    template<typename List>
    using unique = reverse_fold<List, list<>, quote<insert>>;
}

#endif // LIBMETA_META_H
