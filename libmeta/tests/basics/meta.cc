#include "libmeta/meta.h"

#include <tuple>
#include <type_traits>

using namespace meta;

using myList = list<char, int, float>;


/*
 * Basics
 * ------
 *
 * id, quote, invoke, defer, list.
 */

static_assert(std::is_same_v<
    myList,
    typename id<myList>::type
>);

static_assert(std::is_same_v<
    int&,
    invoke<quote<std::add_lvalue_reference_t>, int>
>);

static_assert(std::is_same_v<
    myList,
    e<defer<list, char, int, float>>
>);

static_assert(3 == myList::size());


/*
 * Compatibility
 * -------------
 *
 * apply.
 */

static_assert(std::is_same_v<
    std::tuple<char, int, float>,
    apply<quote<std::tuple>, myList>
>);

static_assert(std::is_same_v<
    list<size_t<3>, size_t<2>, size_t<1>>,
    apply<quote<list>, std::index_sequence<3, 2, 1>>
>);


/*
 * Invocation
 * ----------
 *
 * not_fn.
 */

static_assert(
    invoke<not_fn<id<std::false_type>>>::value
);

/*
 * List handling
 * -------------
 *
 * front, pop_front, second, concat, join, repeat_n, transform, at,
 * push_front, push_back, reverse_fold, filter, find_index, find_if.
 */


static_assert(std::is_same_v<char, front<myList>>);
static_assert(std::is_same_v<list<int, float>, pop_front<myList>>);
static_assert(std::is_same_v<int, second<myList>>);

static_assert(std::is_same_v<
    list<char, int, float, char, int, float>,
    concat<myList, myList>
>);

static_assert(std::is_same_v<
    concat<myList, myList>,
    join<list<myList, myList>>
>);

static_assert(std::is_same_v<
    list<int, int, int>,
    repeat_n_c<3, int>
>);

static_assert(std::is_same_v<
    list<char*, int*, float*>,
    transform<myList, quote<std::add_pointer_t>>
>);

static_assert(std::is_same_v<int, at_c<myList, 1>>);

static_assert(std::is_same_v<
    list<bool, char, int, float>,
    push_front<myList, bool>
>);

static_assert(std::is_same_v<
    list<char, int, float, double>,
    push_back<myList, double>
>);

static_assert(std::is_same_v<
    list<float, int, char>,
    reverse_fold<myList, list<>, quote<push_back>>
>);

static_assert(std::is_same_v<
    list<char, int>,
    filter<myList, quote<std::is_integral>>
>);

static_assert(0 == find_index<myList, char>::value);
static_assert(1 == find_index<myList, int>::value);
static_assert(2 == find_index<myList, float>::value);
static_assert(std::numeric_limits<std::size_t>::max() == find_index<myList, double>::value);
static_assert(std::numeric_limits<std::size_t>::max() == find_index<list<>, void>::value);

static_assert(std::is_same_v<
    list<float>,
    find_if<myList, quote<std::is_floating_point>>
>);

static_assert(std::is_same_v<
    list<>,
    find_if<myList, quote<std::is_pointer>>
>);


/*
 * Set handling
 * ------------
 *
 * inherit, contains, insert, unique.
 */

static_assert(std::is_base_of_v<
    std::tuple<>,
    inherit<list<std::tuple<>, std::tuple<int>>>
>);

static_assert(std::is_base_of_v<
    std::tuple<int>,
    inherit<list<std::tuple<>, std::tuple<int>>>
>);

static_assert(contains<myList, int>::value);
static_assert(!contains<myList, double>::value);

static_assert(std::is_same_v<myList, insert<myList, int>>);

static_assert(std::is_same_v<
    push_front<myList, double>,
    insert<myList, double>
>);

static_assert(std::is_same_v<myList, unique<concat<myList, myList>>>);
