#include "libmeta/meta.h"
#include "libmeta/metamap.h"

#include <type_traits>

using namespace metamap;

using myMap = meta::list<
    meta::list<char, char&>,
    meta::list<int, unsigned int>,
    meta::list<float, float*>
>;

/*
 * Map handling
 * ------------
 *
 * find, contains, insert, update.
 */

static_assert(std::is_same_v<meta::list<int, unsigned int>, find<myMap, int>>);
static_assert(std::is_same_v<void, find<myMap, double>>);

static_assert(contains<myMap, float>);
static_assert(!contains<myMap, void>);

static_assert(std::is_same_v<
    myMap,
    insert<myMap, meta::list<int>>
>);

static_assert(std::is_same_v<
    meta::push_front<myMap, meta::list<void>>,
    insert<myMap, meta::list<void>>
>);

static_assert(std::is_same_v<
    meta::list<
        meta::list<char, char&>,
        meta::list<int>,
        meta::list<float, float*>
    >,
    update<myMap, meta::list<int>>
>);

static_assert(std::is_same_v<
    meta::push_front<myMap, meta::list<void>>,
    update<myMap, meta::list<void>>
>);

static_assert(std::is_same_v<
    meta::list<
        meta::list<char, char&>,
        meta::list<int, unsigned int>*,
        meta::list<float, float*>
    >,
    update<myMap, meta::list<int>, meta::quote<std::add_pointer_t>>
>);
